stages:
  - build
  - lint
  - test
  - deploy

# =============================== STAGE BUILD

.template_build: &template_build
  stage: build
  image: node:8-alpine

build:node_modules:
  <<: *template_build
  script:
    - yarn install
  cache:
    policy: push
    paths:
      - ./node_modules
  except:
    - master
    - demo
    - tags

build:app:
  <<: *template_build
  before_script:
    - if [ ! -z "${CI_COMMIT_TAG}" ]; then npm version ${CI_COMMIT_TAG:1}; fi
  script:
    - yarn install
    - yarn build
  after_script:
    - cp package.json dist/package.json
  cache:
    policy: push
    paths:
      - ./dist
      - ./node_modules
  only:
    - demo
    - tags
  except:
    - master

# =============================== STAGE LINT & TEST

.template_lint_and_test: &template_lint_and_test
  image: node:8-alpine
  cache:
    paths:
      - ./node_modules
    policy: pull
  when: on_success
  except:
    - tags
    - master

lint:js:
  <<: *template_lint_and_test
  stage: lint
  script:
    - yarn lint:js

lint:scss:
  <<: *template_lint_and_test
  stage: lint
  script:
    - yarn lint:scss

test:unit:
  <<: *template_lint_and_test
  stage: test
  script:
    - yarn test:unit

test:e2e:
  <<: *template_lint_and_test
  image: cypress/base:8
  stage: test
  cache: {}
  script:
    - yarn install
    - yarn cypress install
    - yarn run test:e2e --headless

# =============================== STAGE DEPLOY

.deploy_template: &deploy_template
  stage: deploy
  image: registry.gitlab.com/ngrevin/gitlab-ci-js/deploy-image
  before_script:
    - echo ${GCP_CREDENTIALS} > /tmp/${CI_PIPELINE_ID}.json
    - gcloud auth activate-service-account --key-file /tmp/$CI_PIPELINE_ID.json
    - envsubst < app.template.yaml > app.yaml
  after_script:
    - rm /tmp/$CI_PIPELINE_ID.json
  cache:
    paths:
      - ./dist
    policy: pull

deploy:demo:
  <<: *deploy_template
  environment:
    name: demo
    url: https://demo-dot-gitlab-ci-js.appspot.com
  script:
    - gcloud --quiet --verbosity=error app deploy ./app.yaml --project=gitlab-ci-js --version=${CI_PIPELINE_ID} --promote --stop-previous-version
  only:
    - demo

deploy:production:
  <<: *deploy_template
  environment:
    name: production
    url: https://production-dot-gitlab-ci-js.appspot.com
  script:
    - gcloud --quiet --verbosity=error app deploy ./app.yaml --project=gitlab-ci-js --version=${CI_PIPELINE_ID} --promote --stop-previous-version
    - git config --global user.name "${GITLAB_USER_LOGIN}"
    - git config --global user.email "${GITLAB_USER_EMAIL}"
    - git remote rm origin
    - git remote add origin https://${GITLAB_USER_LOGIN}:${PERSONAL_ACCESS_TOKEN}@gitlab.com/ngrevin/gitlab-ci-js.git
    - git fetch --all
    - git checkout demo
    - cp dist/package.json .
    - NEW_VERSION=$(yarn versions | grep gitlab-ci-js | sed "s/\('\| \|,\)//g" | sed -r "s/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[mGK]//g" | cut -d":" -f2)
    - git add --all
    - git commit -m "NEW VERSION - v${NEW_VERSION}"
    - git push https://${GITLAB_USER_LOGIN}:${PERSONAL_ACCESS_TOKEN}@gitlab.com/ngrevin/gitlab-ci-js.git demo
    - git merge -s ours origin/master -m "Merge for new version"
    - git checkout master
    - git merge --no-ff --commit -m "NEW VERSION - v${NEW_VERSION}" demo
    - git push https://${GITLAB_USER_LOGIN}:${PERSONAL_ACCESS_TOKEN}@gitlab.com/ngrevin/gitlab-ci-js.git master
  artifacts:
    name: "${CI_COMMIT_TAG:1}"
    untracked: true
    paths:
      - .
  only:
    - tags
