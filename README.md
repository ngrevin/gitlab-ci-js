# gitlab-ci-js

## Command

```bash
make start              # Start project
make stop               # Stop project
make destroy            # Remove project container's
make logs               # Follow app log's 
make sh                 # Connect to container with sh
make npm                # Run npm inside app container's
make yarn               # Run yarn inside app container's
make exec               # Exec a command inside app container's
make interact           # Exec a interactive command inside app container's 
make run                # Exec a command inside stopped container's
make e2e                # Run e2e test
make unit               # Run unit test
make lint-js            # Run lint js
make link-scss          # Run lint scss
```
